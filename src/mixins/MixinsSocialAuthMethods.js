export const mixinsSocialAuthMethods = {
  methods: {

    onAuthLoginSuccess(user) {
      this.$emit('success', user)
    },

    onAuthLoginFailure(error) {
      this.$emit('error', error)
    },

    loginHello(social) {
      hello(social)
        .login({
          scope: 'email'
        })
        .then(() => {

          hello(social)
            .api('me')
            .then((response) => {
              this.onAuthLoginSuccess({
                name: response.name,
                email: response.email,
                password: response.id + response.email.split('@')[0],
                id: response.id,
                isRemember: true
              })
            })

        })
    },

    isLogin(social) {
      let session = hello(social)
        .getAuthResponse();
      let currentTime = (new Date())
        .getTime() / 1000;
      return session && session.access_token && session.expires > currentTime;
    },

    authenticate(social) {
      if (this.isLogin(social)) {
        hello(social)
          .logout()
          .then(() => {
            this.loginHello(social)
          });
      } else {
        this.loginHello(social)
      }
    }

  }
}