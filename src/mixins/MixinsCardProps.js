export const MixinsCardProps = {
  props: {
    image: {
      type: String,
      required: true
    },
    id: {
      types: [String, Number],
      required: true
    },
    title: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    isContentCenter: {
      type: Boolean,
      required: false,
      default: true
    },
    rating: {
      type: [String, Number],
      required: false
    },
    date: {
      type: [String, Number],
      required: false
    }
  }
}