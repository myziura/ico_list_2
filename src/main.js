import Vue from 'vue'

import {
  router
} from './router/routes'

import App from './App'

// Utils
import UtilsString from './utils/UtilsString'
import UtilsArray from './utils/UtilsArray'

// Lodash
import {
  isEqual
} from 'lodash'

// Validation
import VeeValidate from 'vee-validate';

// Basic element-ui theme
// import 'element-ui/lib/theme-chalk/index.css';

import ElementUI from 'element-ui'
// Custom element-ui theme
import '../src/assets/element-variables.scss'

import store from './store/index'

import locale from 'element-ui/lib/locale/lang/en'

import {
  Autocomplete,
  Menu,
  MenuItem,
  Input,
  InputNumber,
  Checkbox,
  Radio,
  RadioGroup,
  RadioButton,
  Select,
  Option,
  Button,
  Card,
  Rate,
  Notification,
  DatePicker,
  Tooltip,
  Steps,
  Step,
  Tabs,
  TabPane,
  Pagination,
  Upload,
  Loading,
  Popover,
} from 'element-ui';

Vue.use(Autocomplete);
Vue.use(Menu);
Vue.use(MenuItem);
Vue.use(Input);
Vue.use(Radio);
Vue.use(RadioGroup);
Vue.use(RadioButton);
Vue.use(InputNumber);
Vue.use(Checkbox);
Vue.use(Select);
Vue.use(Option);
Vue.use(Button);
Vue.use(Card);
Vue.use(Rate);
Vue.use(DatePicker);
Vue.use(Tooltip);
Vue.use(Steps);
Vue.use(Step);
Vue.use(Tabs);
Vue.use(TabPane);
Vue.use(Upload);
Vue.use(Pagination);
Vue.use(Popover);

Vue.use(Loading.directive);

Vue.use(ElementUI, {
  locale
});

// Social
hello.init({
  facebook: '257223624914944',
  google: '509287225155-pacbc5t0vp01ncttniqvgn72t4lafgao.apps.googleusercontent.com'
}, {
  redirect_uri: location.protocol + "//" + location.host
});

Vue.use(VeeValidate);
// Vue.use(locale);

Vue.prototype.$loading = Loading;
Vue.prototype.$notify = Notification;
Vue.prototype.router = router;

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
  })
  .$mount('#app')