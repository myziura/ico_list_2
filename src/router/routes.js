import Vue from 'vue'
import VueRouter from 'vue-router'

import ScreenAppMain from '../screens/ScreenAppMain'


import ScreenAuntification from '../screens/ScreenAuntification'
import ScreenLogIn from '../screens/ScreenLogIn'
import ScreenSignUp from '../screens/ScreenSignUp'
import ScreenPasswordReset from '../screens/ScreenPasswordReset'
import ScreenAccountActivate from '../screens/ScreenAccountActivate'

import ScreenPublishICO from '../screens/ScreenPublishICO'
import ScreenICOList from '../screens/ScreenICOList'
import ScreenICO from '../screens/ScreenICO'

import ScreenBlog from '../screens/ScreenBlog'
import ScreenBlogPost from '../screens/ScreenBlogPost'
import ScreenCreatePost from '../screens/ScreenCreatePost'

import ScreenExpertList from '../screens/ScreenExpertList'
import ScreenExpert from '../screens/ScreenExpert'

import ScreenForumList from '../screens/ScreenForumList'
import ScreenForumPost from '../screens/ScreenForumPost'

import ScreenPolicy from '../screens/ScreenPolicy'

import ScreenPriceList from '../screens/ScreenPriceList'
import ScreenPay from '../screens/ScreenPay'

//Admin
import ScreenAdminValidateListICO from '../screens/admin/ScreenAdminValidateListICO'
import ScreenAdminValidateICO from '../screens/admin/ScreenAdminValidateICO'

//Profile
import ScreenAccount from '../screens/ScreenAccount'

Vue.use(VueRouter)

const routes = [

  // Global
  {
    path: '*',
    redirect: '/'
  }, {
    name: '/',
    path: '/',
    component: ScreenAppMain
  },

  // Authentication
  {
    name: 'auntification',
    // path: '/auntification/:component',
    path: '/auntification',
    component: ScreenAuntification,
    children: [
      {
        name: 'login',
        path: 'login',
        component: ScreenLogIn
      }, 
      {
        name: 'sign-up',
        path: 'sign-up',
        component: ScreenSignUp
      },
    ]
  },

  // {
  //   name: 'login',
  //   path: '/login',
  //   component: ScreenLogIn
  // }, 
  // {
  //   name: 'sign-up',
  //   path: '/sign-up',
  //   component: ScreenSignUp
  // },
  {
    path: '/password-reset',
    component: ScreenPasswordReset
  }, {
    path: '/active/:token',
    component: ScreenAccountActivate
  },

  // Publish ICO
  {
    name: "publish-ico",
    path: '/ico-publish',
    component: ScreenPublishICO
  },
  {
    name: "publish-ico-tarriff",
    path: '/ico-publish/tariff',
    component: ScreenPublishICO
  },

  {
    name: "ico-list",
    path: '/ico/list/:page?',
    component: ScreenICOList,
  },
  {
    name: 'ico',
    path: '/ico/:id',
    component: ScreenICO
  },

  // Extert
  {
    name: "expert-list",
    path: "/expert/list",
    component: ScreenExpertList,
  },
  {
    name: "expert",
    path: "/expert/:id",
    component: ScreenExpert,
  },

  // Forum
  {
    name: "forum-themes",
    path: "/forum/themes/:page?",
    component: ScreenForumList,
  },
  {
    name: "forum-post",
    path: "/forum/post/:id",
    component: ScreenForumPost,
  },

  // Blog
  {
    name: 'blog',
    path: '/blog',
    component: ScreenBlog
  }, {
    path: '/post/:id',
    component: ScreenBlogPost
  }, {
    name: 'create.post',
    path: '/create/post/:type',
    component: ScreenCreatePost
  },

  // Polisy (roots)
  {
    name: "polisy",
    path: '/polisy/:polisyName',
    component: ScreenPolicy
  },

  // Payment (PriceList)
  {
    name: 'payment',
    path: '/payment',
    component: ScreenPriceList
  }, 
  {
    path: '/payment/pay/:rate',
    component: ScreenPay
  },

  // Admin
  {
    path: '/admin/validate/ico/list',
    component: ScreenAdminValidateListICO
  },
  {
    path: '/admin/validate/ico',
    component: ScreenAdminValidateICO
  },

  //Profile
  {
    path: '/account/:page',
    component: ScreenAccount
  },

]


export const router = new VueRouter({
  mode: 'history',
  routes,
  // возвращаем требуемую позицию прокрутки
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return {
        x: 0,
        y: 0
      }
    }
  }
})