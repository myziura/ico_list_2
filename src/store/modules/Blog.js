/* Blog Vuex module
 * - Create post (createPost);
 * - Get all posts (loadPosts)
 * - Get one post (loadSelectPost)
 */

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import * as types from '../mutation-types'

Vue.use(Vuex)

const state = {
  //Post
  posts: [],
  selectPost: {}
};

const getters = {

  // User
  posts: state => state.posts,
  post: state => {
    return id => {
      return state.posts.find(post => post.id === parseInt(id))
    }
  },
  currentPost: state => state.selectPost.current,
  prevPost: state => state.selectPost.prev,
  nextPost: state => state.selectPost.next
};

const actions = {
  createPost({
    commit,
    rootState,
  }, data) {

    console.log(data);

    const url = rootState.Auth.domen + 'create/post'
    const auth = rootState.Auth.token_type + rootState.Auth.token

    axios.post(url, data, {
        headers: {
          Authorization: auth
        }
      })
      .then(response => {

        console.log(response);
        if (response.data.success == 'true') {

          // ad info about post (id, title, short_description, image, created_at, content)
          commit(types.ADD_POST, data)

          // Notification about successfyl create post
          Vue.prototype.$notify({
            title: 'Success',
            message: 'You have successfully added a post',
            type: 'success',
            position: rootState.Auth.notify_position
          })

          Vue.prototype.$router.push('/blog')

        } else {
          // Notification about error create post
          Vue.prototype.$notify.error({
            title: 'Error',
            message: response.data.error,
            position: rootState.Auth.notify_position
          })
        }

        // Stoping loading button in create post form
        commit(types.SAVE_LOADING_STATE, false)
      })
      .catch(error => {

        // Stoping loading button in create post form
        commit(types.SAVE_LOADING_STATE, false)
      })
  },

  loadPosts({
    commit,
    rootState
  }, id) {

    let url = rootState.Auth.domen + 'get/post/all'

    axios.get(url)
      .then(response => {

        // Save info about post (id, title, short_description, image, created_at, content)
        commit(types.SAVE_POSTS, response.data.data)

        // Stoping loading
        commit(types.SAVE_LOADING_STATE, false)
      })
      .catch(error => {

        // Stoping loading
        commit(types.SAVE_LOADING_STATE, false)
      })
  },

  loadSelectPost({
    commit,
    rootState
  }, id) {

    let url = rootState.Auth.domen + 'get/post/' + id

    axios.get(url)
      .then(response => {

        if (response.data.success == 'true') {
          // Save info about post (id, title, short_description, image, created_at, content)
          commit(types.SAVE_SELECT_POST, response.data.data)
        } else {
          // Go to blog page
          Vue.prototype.router.push('/blog')

          // Notification about error load post
          Vue.prototype.$notify.error({
            title: 'Error',
            message: response.data.error,
            position: rootState.Auth.notify_position
          })
        }

        // Stoping loading
        commit(types.SAVE_LOADING_STATE, false)
      })
      .catch(error => {
        // Stoping loading
        commit(types.SAVE_LOADING_STATE, false)
      })
  }
};

const mutations = {
  [types.ADD_POST](state, post) {
    state.posts.push(post)
  },
  [types.SAVE_POSTS](state, posts) {
    state.posts = posts
  },
  [types.SAVE_SELECT_POST](state, selectPost) {
    state.selectPost = selectPost
  }
};


export default {
  state,
  getters,
  actions,
  mutations
};