/* ICO Vuex module
 * - Get categories (loaddingCategories);
 */

import Vue from "vue"
import axios from 'axios'
import * as types from '../mutation-types'

const state = {
  categories: [],
  loadingPage: false
};

const getters = {
  categories: state => state.categories,
};

const actions = {
  loaddingCategories({
    state,
    rootState,
    rootGetters,
    commit
  }) {
    const {
      domen,
      token
    } = rootState.Auth

    const url = `${domen}get/icoCategory/all`
    const authorization = `Bearer ${token}`

    axios.get(url, {
        headers: {
          Authorization: authorization
        }
      })
      .then(response => {
        // console.log(response.data.data);
        commit(types.SAVE_CATIGORIES, response.data.data)

        // if (totalICO == 0)
        // commit(types.SAVE_TOTAL_ICO, response.data.data.total)
      })
      .catch(error => {
        Vue.prototype.$notify.error({
          title: 'Error',
          message: 'Не удалось загрузить данные',
          position: rootState.Auth.notify_position
        })
      })
  },
};

const mutations = {
  [types.SAVE_CATIGORIES](state, categories) {
    state.categories = categories;
  },

  [types.SAVE_LOADING_PAGE_STATE](state, isLoad) {

    if (isLoad) Vue.prototype.$loading.service()
    if (!isLoad && state.loadingPage != false) setTimeout(() => {
      Vue.prototype.$loading.service()
        .close()
    }, 500)

    state.loadingPage = isLoad
  }
};


export default {
  state,
  getters,
  actions,
  mutations
};