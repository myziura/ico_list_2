/* Forum Vuex module
 * - Get forum theme list info (getForumThemesList);
 * - Get forum data about one post (getForumPostData)
 */

import Vue from "vue"
import axios from 'axios'
import * as types from '../mutation-types'

const state = {
  forumThemesList: [],
  forumPostData: {},
  forumTotalCountPosts: 0,
  forumFilterOptions: {
    sortText: '',
    text_search: '',
    categories: []
  }
};

const getters = {
  forumThemesList: state => state.forumThemesList,
  forumPostData: state => state.forumPostData,
  forumTotalCountPosts: state => state.forumTotalCountPosts,
  forumFilterOptions: state => state.forumFilterOptions
};

const actions = {

  /* NOTE: GET form data
   **********************/
  getForumThemesList({
    commit,
    rootState,
  }, page = 1) {

    const url = rootState.Auth.domen + 'forum/get/post/all'

    let filter = {
      ...rootState.Forum.forumFilterOptions,
      page: page
    }

    // Preloader
    commit(types.SAVE_LOADING_PAGE_STATE, true)

    axios.post(url, filter)
      .then(response => {
        if (response.data.success == 'true') {

          commit(types.SAVE_FORUM_THEMES_LIST, response.data.data)

          // Preloader
          commit(types.SAVE_LOADING_PAGE_STATE, false)
        }
      })
      .catch(error => {
        console.log(error);

        // Stoping loading button in create post form
        commit(types.SAVE_LOADING_STATE, false)
      })
  },

  getForumPostData({
    commit,
    rootState
  }, id) {

    const url = rootState.Auth.domen + 'forum/get/post/' + id

    // Preloader
    commit(types.SAVE_LOADING_PAGE_STATE, true)

    axios.get(url)
      .then(response => {
        if (response.data.success == 'true') {
          console.log(response.data);
          commit(types.SAVE_FORUM_POST_DATA, response.data.data)

          // Preloader
          commit(types.SAVE_LOADING_PAGE_STATE, false)
        }
      })
      .catch(error => {
        console.log(error);

        // Stoping loading button in create post form
        commit(types.SAVE_LOADING_STATE, false)
      })
  },

  /* NOTE: POST edit
   **********************/

  createForumPost({
    commit,
    rootState
  }, data) {

    const url = rootState.Auth.domen + 'forum/create/post'
    const auth = rootState.Auth.token_type + rootState.Auth.token

    // Preloader
    commit(types.SAVE_LOADING_PAGE_STATE, true)

    axios.post(url, data, {
        headers: {
          Authorization: auth
        }
      })
      .then(response => {
        if (response.data.success == 'true') {
          Vue.prototype.router.push('/forum/themes')

          // Notification about successfyl registration
          Vue.prototype.$notify({
            title: 'Success',
            message: 'Topic has been added',
            type: 'success',
            position: rootState.Auth.notify_position
          })
        }

        // Stoping loading button in create post form
        commit(types.SAVE_LOADING_STATE, false)
      })
      .catch(error => {
        console.log(error);

        // Stoping loading button in create post form
        commit(types.SAVE_LOADING_STATE, false)
      })
  },

  updateForumPost({
    commit,
    rootState
  }, id) {

    const url = rootState.Auth.domen + 'forum/update/post' + id

    axios.post(url)
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      })
  },

  deleteForumPost({
    commit,
    rootState
  }, id) {

    const url = rootState.Auth.domen + 'forum/delete/post' + id

    axios.delete(url)
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      })
  },

  /* NOTE: COMMENTS edit
   **********************/

  createForumComment({
    commit,
    rootState,
    dispatch
  }, data) {

    const url = rootState.Auth.domen + 'forum/create/comment'
    const auth = rootState.Auth.token_type + rootState.Auth.token

    // Starting loading button in create post form
    commit(types.SAVE_LOADING_STATE, true)

    axios.post(url, data, {
        headers: {
          Authorization: auth
        }
      })
      .then(response => {
        if (response.data.success == 'true') {

          // Notification about successfyl registration
          Vue.prototype.$notify({
            title: 'Success',
            message: 'Comment has been added',
            type: 'success',
            position: rootState.Auth.notify_position
          })

          // Stoping loading button in create post form
          commit(types.SAVE_LOADING_STATE, false)
          dispatch('getForumPostData', location.href.split('post/')[1])
        }
      })
      .catch(error => {
        console.log(error);

        // Notification about successfyl registration
        Vue.prototype.$notify({
          title: 'Error',
          message: 'Something went wrong',
          type: 'error',
          position: rootState.Auth.notify_position
        })

        // Stoping loading button in create post form
        commit(types.SAVE_LOADING_STATE, false)
      })
  },

  updateForumComment({
    commit,
    rootState,
    dispatch
  }, data) {

    console.log(data);

    const url = rootState.Auth.domen + 'forum/update/comment/' + data.id
    const auth = rootState.Auth.token_type + rootState.Auth.token

    // Starting loading button in create post form
    commit(types.SAVE_LOADING_STATE, true)

    axios.post(url, {
        content: data.content
      }, {
        headers: {
          Authorization: auth
        }
      })
      .then(response => {
        if (response.data.success == 'true') {

          // Notification about successfyl registration
          Vue.prototype.$notify({
            title: 'Success',
            message: 'Comment was changed',
            type: 'success',
            position: rootState.Auth.notify_position
          })

          // Stoping loading button in create post form
          commit(types.SAVE_LOADING_STATE, false)
          dispatch('getForumPostData', location.href.split('post/')[1])
        }
      })
      .catch(error => {
        console.log(error);

        // Notification about successfyl registration
        Vue.prototype.$notify({
          title: 'Error',
          message: 'Something went wrong',
          type: 'error',
          position: rootState.Auth.notify_position
        })

        // Stoping loading button in create post form
        commit(types.SAVE_LOADING_STATE, false)
      })
  },

  deleteForumComment({
    commit,
    rootState,
    dispatch
  }, id) {

    const url = rootState.Auth.domen + 'forum/delete/comment/' + id
    const auth = rootState.Auth.token_type + rootState.Auth.token

    axios.delete(url, {
        headers: {
          Authorization: auth
        }
      })
      .then(response => {
        if (response.data.success == 'true') {

          // Notification about successfyl registration
          Vue.prototype.$notify({
            title: 'Success',
            message: 'Comment was deleted',
            type: 'success',
            position: rootState.Auth.notify_position
          })

          // Stoping loading button in create post form
          commit(types.SAVE_LOADING_STATE, false)
          dispatch('getForumPostData', location.href.split('post/')[1])
        }
      })
      .catch(error => {
        console.log(error);

        // Notification about successfyl registration
        Vue.prototype.$notify({
          title: 'Error',
          message: 'Something went wrong',
          type: 'error',
          position: rootState.Auth.notify_position
        })

        // Stoping loading button in create post form
        commit(types.SAVE_LOADING_STATE, false)
      })
  },

  // NOTE: OTHER opportunities

  likeForumPost({
    commit,
    rootState,
    dispatch
  }, id) {

    const url = rootState.Auth.domen + 'forum/like/post/' + id
    const auth = rootState.Auth.token_type + rootState.Auth.token

    axios.get(url, {
        headers: {
          Authorization: auth
        }
      })
      .then(response => {
        console.log('--------------------------');
        console.log(response.data.success);
        // commit('SAVE_FORUM_IS_LIKE', response.data.success)
        dispatch('getForumPostData', location.href.split('post/')[1])
      })
      .catch(error => {
        console.log(error);
      })
  },

};

const mutations = {
[types.SAVE_FORUM_THEMES_LIST](state, data) {
    state.forumThemesList = data.data
    state.forumTotalCountPosts = data.total
  },
[types.SAVE_FORUM_POST_DATA](state, data) {
    state.forumPostData = data
  },
[types.SAVE_FORUM_FILTER_OPTIONS](state, data) {
    state.forumFilterOptions = data
  },
[types.SAVE_FORUM_IS_LIKE](state, isLike) {
    state.isLike = !isLike
  }
};


export default {
  state,
  getters,
  actions,
  mutations
};