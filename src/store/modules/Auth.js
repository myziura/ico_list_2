/* Authorization Vuex module
 * - Registration (registration);
 * - Login (login);
 * - Geting info about user (getUserData);
 * - logout (logout);
 * - Password restore (passwordRestore).
 */

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import * as types from '../mutation-types'

Vue.use(Vuex)

const state = {

  // Global
  domen: 'https://ccnowpro.com/icoList/public/api/',
  token_type: 'Bearer ',
  client_id: '2',
  client_secret: 'NOVfqoTBQkwNhwj8qjTHvHAnBLKJCWDOXQ4H89w7',
  client_url: location.protocol + "//" + location.host,

  // User
  is_authenticated: false,
  token: localStorage.getItem('user-token') || '',
  email: '',
  user_id: '',
  is_activate: '',
  name: '',
  image: '',
  role: '',

  //route
  nextPage: null,

  // Interface
  loading: false,
  notify_position: 'bottom-right',
};

const getters = {

  // User
  is_authenticated: state => state.is_authenticated,
  name: state => state.name,
  email: state => state.email,
  image: state => state.image,
  role: state => state.role,
  user_id: state => state.user_id,

  //route
  nextPage: ({ nextPage }) => nextPage,

  // Interface
  loading: state => state.loading,
  notify_position: state => state.notify_position
};

const actions = {

  registration({
    commit
  }, data) {

    // Adding vital data
    data.client_id = state.client_id
    data.client_secret = state.client_secret
    data.client_url = state.client_url

    let url = state.domen + 'register'

    // Notification about successfyl registration
    Vue.prototype.$notify({
      title: 'Info',
      message: 'Please, wait a second...',
      type: 'info',
      position: state.notify_position
    })

    axios.post(url, data)
      .then(response => {

        if (response.data.success == 'true') {

          // Notification about successfyl registration
          Vue.prototype.$notify({
            title: 'Success',
            message: 'You have successfully registered',
            type: 'success',
            position: state.notify_position
          })

          // Go to login form
          Vue.prototype.router.push('/login')

        } else {

          // Notification about error while registration
          Vue.prototype.$notify.error({
            title: 'Error',
            message: response.data.error,
            position: state.notify_position
          })
        }

        // Stoping loading button in registration form
        commit(types.SAVE_LOADING_STATE, false)
      })
      .catch(error => {

        // Stoping loading button in registration form
        commit(types.SAVE_LOADING_STATE, false)
      })
  },

  login({
    commit,
    dispatch
  }, data) {

    // Adding vital data
    data.client_id = state.client_id
    data.client_secret = state.client_secret
    data.client_url = state.client_url

    let url = state.domen + 'oauth/token'

    // Notification about successful authorize
    Vue.prototype.$notify({
      title: 'Info',
      message: 'Please, wait a second...',
      type: 'info',
      position: state.notify_position
    })

    axios.post(url, data)
      .then(response => {

        if (response.data.success == 'true') {

          // Notification about successful authorize
          Vue.prototype.$notify({
            title: 'Success',
            message: 'You have been successfully authorized',
            type: 'success',
            position: state.notify_position
          })

          // Save token
          commit(types.SAVE_USER_TOKEN, response.data.data.access_token)

          localStorage.setItem('user-token', response.data.data.access_token)

          // // Go to Heropage
          // Vue.prototype.router.push('/')
          // Vue.prototype.router.go(-1)


          // Get user data
          dispatch('getUserData')

        } else {

          // Notification about error while authorize
          Vue.prototype.$notify.error({
            title: 'Error',
            message: response.data.error,
            position: state.notify_position
          })
        }

        // Stoping loading button in registration form
        commit(types.SAVE_LOADING_STATE, false)
      })
      .catch(error => {

        // Stoping loading button in registration form
        commit(types.SAVE_LOADING_STATE, false)
      })
  },

  getUserData({
    commit
  }) {

    let token = state.token

    if (!token) return

    let url = state.domen + 'get/user'
    let authorization = 'Bearer ' + token

    axios.get(url, {
        headers: {
          Authorization: authorization
        }
      })
      .then(response => {

        // User greeting
        Vue.prototype.$notify({
          title: 'Success',
          message: 'Hello, ' + response.data.name,
          type: 'success',
          position: state.notify_position
        })

        // Save info about user (email, id, mail confirm, name, role)
        commit(types.SAVE_USER_INFO, response.data)
      })
      .catch(error => {

        // Notification about error while authorize
        Vue.prototype.$notify.error({
          title: 'Error',
          message: 'Invalid user data',
          position: state.notify_position
        })
      })
  },

  updataPassword({
    commit
  }, data) {

    console.log(data);

    let token = state.token

    if (!token) return

    let url = state.domen + 'udate/password/user'
    let authorization = 'Bearer ' + token

    axios.post(url, data, {
        headers: {
          Authorization: authorization
        }
      })
      .then(response => {

        if (response.data.success == 'true') {

          // Notification about successfyl update password
          Vue.prototype.$notify({
            title: 'Success',
            message: 'Password updated',
            type: 'success',
            position: state.notify_position
          })

        } else {
          // Notification about error create post
          Vue.prototype.$notify.error({
            title: 'Error',
            message: response.data.error,
            position: state.notify_position
          })
        }
      })
      .catch(error => {

        // Notification about error while authorize
        // Vue.prototype.$notify.error({
        //   title: 'Error',
        //   message: 'Invalid user data',
        //   position: state.notify_position
        // })
      })
  },

  updataInfo({
    commit
  }, data) {

    let token = state.token

    if (!token) return

    let url = state.domen + 'udate/name/user'
    let authorization = 'Bearer ' + token

    axios.post(url, data, {
        headers: {
          Authorization: authorization
        }
      })
      .then(response => {
        console.log(response);

        if (response.data.success == 'true') {

          // Notification about successfyl update name
          Vue.prototype.$notify({
            title: 'Success',
            message: 'Profile updated',
            type: 'success',
            position: state.notify_position
          })

          // save user name
          commit(types.UPDATE_USER_INFO, response.data.data)

        } else {
          // Notification about error create post
          Vue.prototype.$notify.error({
            title: 'Error',
            message: response.data.error,
            position: state.notify_position
          })
        }
      })
      .catch(error => {

        // Notification about error while authorize
        // Vue.prototype.$notify.error({
        //   title: 'Error',
        //   message: 'Invalid user data',
        //   position: state.notify_position
        // })
      })
  },

  logout({
    commit
  }) {

    // Notification about successfyl exit
    Vue.prototype.$notify({
      title: 'Success',
      message: state.name + ', we will miss you',
      type: 'success',
      position: state.notify_position
    })

    commit(types.DELETE_USER_INFO)
  }

};

const mutations = {
  [types.SAVE_LOADING_STATE](state, loading) {
    state.loading = loading
  },

  [types.SAVE_USER_TOKEN](state, token) {
    state.token = token
  },

  [types.SAVE_USER_INFO](state, info) {
    state.email = info.email
    state.user_id = info.id
    state.is_activate = info.is_activate
    state.name = info.name
    state.image = info.image
    state.role = info.role

    console.log(info);

    state.is_authenticated = true
  },

  [types.DELETE_USER_INFO](state) {
    localStorage.clear()
    state.is_authenticated = false
    state.email = ''
    state.user_id = ''
    state.is_activate = ''
    state.name = ''
    state.role = ''
  },

  [types.UPDATE_USER_INFO](state, data) {
    console.log(data);
    state.name = data.name
    state.image = data.image
  },

  [types.SAVE_NEXT_PAGE](state, nextPage) {
    state.nextPage = nextPage;
  },
};


export default {
  state,
  getters,
  actions,
  mutations
};
