/* ICO Vuex module
 * - Get ICOs (loaddingICOList);
 * - Get one ICO (loaddingICO)
 */

import Vue from "vue"
import axios from 'axios'
import * as types from '../mutation-types'

const state = {
  categories: [],
  ICOList: [],
  ICOListTop: [],
  ICOUserList: [],
  totalICO: 0,
  icoData: null,
  filters: {
    "categories": [],
    "text_search": '',
  },
  currentFilter: 'rating'
};

const getters = {
  ICOList: state => state.ICOList,
  ICOListTop: state => state.ICOListTop,
  ICOUserList: state => state.ICOUserList,
  totalICO: state => {
    return state.totalICO;
  },
  icoData: state => state.icoData,
  filters: state => state.filters,
};

const actions = {

  loaddingICOList({
    state,
    rootState,
    getters,
    rootGetters,
    commit
  }, page) {

    const {
      totalICO
    } = rootGetters;

    const {
      filters
    } = getters;

    const {
      domen
    } = rootState.Auth;

    const url = `${domen}get/icoList/all`

    let info = {
      page: page + '',
      "categories": filters["categories"],
      "text_search": filters["text_search"],
    };
    if (!info.sortText) info.sortText = state.currentFilter

    // Preloader
    commit(types.SAVE_LOADING_PAGE_STATE, true)

    console.log(info);

    if (page)
      axios.post(url, info)
      .then(response => {

        // Check if response successful
        if (response.data.success == 'true') {
          let data = response.data.data

          // Preloader
          commit(types.SAVE_LOADING_PAGE_STATE, false)

          // Saving response data about ICOs (20)
          commit(types.SAVE_ICO_LIST, data.data)

          // Saving total count of all ICOs
          if (data.total)
            commit(types.SAVE_TOTAL_ICO, data.total)
        }
      })
      .catch(error => {
        console.log(error);
        Vue.prototype.$notify.error({
          title: 'Error',
          message: 'Не удалось загрузить данные',
          position: rootState.Auth.notify_position
        })

        // Preloader
        commit(types.SAVE_LOADING_PAGE_STATE, false)
      })
  },


  loaddingICOListTop({
    commit,
    rootState
  }, page) {

    const {
      domen
    } = rootState.Auth;

    const url = `${domen}get/icoList/all`

    let info = {};
    info.page = page + ''
    if (!info.sortText) info.sortText = state.currentFilter

    // Preloader
    commit(types.SAVE_LOADING_PAGE_STATE, true)

    if (page)
      axios.post(url, info)
      .then(response => {

        // Check if response successful
        if (response.data.success == 'true') {
          let data = response.data.data

          // Preloader
          commit(types.SAVE_LOADING_PAGE_STATE, false)

          // Saving top 5 ICOs by rating
          commit(types.SAVE_ICO_LIST_TOP, data.data.slice(0, 6))
        }
      })
      .catch(error => {
        console.log(error);
        Vue.prototype.$notify.error({
          title: 'Error',
          message: 'Не удалось загрузить данные топа',
          position: rootState.Auth.notify_position
        })

        // Preloader
        commit(types.SAVE_LOADING_PAGE_STATE, false)
      })
  },

  loaddingICO({
    rootState,
    commit
  }, id) {

    const {
      domen
    } = rootState.Auth;

    const url = `${domen}get/icoList/${id}`;

    commit(types.SAVE_LOADING_PAGE_STATE, true)

    axios.get(url)
      .then(response => {
        commit(types.SAVE_ICO_DATA, response.data)

        commit(types.SAVE_LOADING_PAGE_STATE, false)
      })
      .catch(error => {
        commit(types.SAVE_LOADING_PAGE_STATE, false)

        Vue.prototype.$notify.error({
          title: 'Error',
          message: 'Не удалось загрузить данные',
          position: rootState.Auth.notify_position
        })
      })
  },

  loaddingUserICO({
    rootState,
    commit
  }) {

    const {
      domen,
    } = rootState.Auth;


    // const url = domen + 'get/user/ico';
    // const auth = rootState.Auth.token_type + rootState.Auth.token

    const url = domen + 'get/user/ico'
    const auth = rootState.Auth.token_type + rootState.Auth.token

    commit(types.SAVE_LOADING_PAGE_STATE, true)

    axios.get(url, {
        headers: {
          Authorization: auth
        }
      })
      .then(response => {
        console.log(response );
        commit(types.SAVE_UER_ICO_DATA, response.data.data)

        commit(types.SAVE_LOADING_PAGE_STATE, false)
      })
      .catch(error => {
        commit(types.SAVE_LOADING_PAGE_STATE, false)

        Vue.prototype.$notify.error({
          title: 'Error',
          message: 'Не удалось загрузить данные',
          position: rootState.Auth.notify_position
        })
      })
  },

  deleteIco({
    commit,
    rootState,
  }, id) {

    //console.log(id);

    const url = rootState.Auth.domen + 'delete/icoList/' + id
    const auth = rootState.Auth.token_type + rootState.Auth.token

    axios.delete(url, {
        headers: {
          Authorization: auth
        }
      })
      .then(response => {

        console.log(response);
        if (response.data.success == 'true') {

          // delete ico
          commit(types.DELETE_ICO, id)

          // Notification about successfyl delete ico
          Vue.prototype.$notify({
            title: 'Success',
            message: 'You have successfully delete ico',
            type: 'success',
            position: rootState.Auth.notify_position
          })

          Vue.prototype.$router.push('/blog')

        } else {
          // Notification about error create post
          Vue.prototype.$notify.error({
            title: 'Error',
            message: response.data.error,
            position: rootState.Auth.notify_position
          })
        }

      })
      .catch(error => {
        
      })
  },
};

const mutations = {
  [types.SAVE_ICO_LIST](state, ICOList) {
    state.ICOList = ICOList;
  },
  [types.SAVE_ICO_LIST_TOP](state, ICOListTop) {
    state.ICOListTop = ICOListTop
  },
  [types.SAVE_TOTAL_ICO](state, totalICO) {
    state.totalICO = totalICO;
  },
  [types.SAVE_ICO_DATA](state, icoData) {
    state.icoData = icoData;
  },
  [types.SAVE_SELECTED_CATEGORIES](state, icoData) {
    state.icoData = icoData;
  },
  [types.SAVA_FILTERS_SEARCH_ICO](state, filters) {

    state.filters = filters;

    console.log(state.filters);
    // state.filters = {
    //   ...state.filters,
    //   ...filters,
    // };
  },

  [types.SAVE_UER_ICO_DATA](state, ICOUserList) {
    state.ICOUserList = ICOUserList;
  },

  [types.DELETE_ICO](state, id) {
    let icoList = []

    state.ICOUserList.forEach((ico, index) => {
      if (ico.id != id) {
        icoList.push(ico)
      }
    })

    state.ICOUserList = icoList
  },
};


export default {
  state,
  getters,
  actions,
  mutations
};