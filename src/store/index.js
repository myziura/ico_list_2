/* Importing Vuex modules
 *************************/

import Vue from 'vue'
import Vuex from 'vuex'
import * as types from './mutation-types'

import Auth from './modules/Auth'
import Blog from './modules/Blog'
import ICOList from './modules/ICOList'
import Main from './modules/Main'
import Forum from './modules/Forum'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    Auth,
    Blog,
    ICOList,
    Main,
    Forum
  }
})